var util = require('util');
var events = require('events');

function ClickWithLog () {}
util.inherits(ClickWithLog, events.EventEmitter);

ClickWithLog.prototype.command = function(object) {
    var command = this;
    // console.log(command)
    command.client.api.click(object, function(result){
      if (result.status === 0) {
        command.client.api.verify.ok('Info', object + " was clicked")
      } else   {
        command.client.api.verify.fail(object + " wasnt clicked. Maybe it not found")
      }
      this.emit('complete');
    }.bind(this));
};

module.exports = ClickWithLog;