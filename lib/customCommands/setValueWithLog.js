var util = require('util');
var events = require('events');

function SetValueWithLog () {}
util.inherits(SetValueWithLog, events.EventEmitter);

SetValueWithLog.prototype.command = function(object, value) {
    var command = this;
    // console.log(command)
    command.client.api.setValue(object , value, function(result){
      if (result.status === 0) {
        command.client.api.verify.ok('Info', "Value - " + value + " was setted. Field - " + object)
      } else   {
        command.client.api.verify.fail( "Value - " + value + " wasnt setted. Maybe field - " + object + " wasnt found")
      }
      this.emit('complete');
    }.bind(this));
};

module.exports = SetValueWithLog;