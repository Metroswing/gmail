var config = require('./config');

function getDateCustomFormat() {
  var date = new Date(),
      dateFormated = ''
      dateFormated =  addZero( date.getDate() ) + '.' 
                      +  addZero( date.getMonth() + 1 ) + '.'
                      + date.getFullYear().toString() + 'T'
                      + date.getHours().toString() + '-'
                      + addZero( date.getMinutes() ) + '-'
                      + addZero( date.getSeconds() )
  return dateFormated;
}

function addZero( num ) {
  return num < 10 ? '0' + num.toString() : num.toString().toString();
}

function initHtmlReport() {
  var htmlReporter = require('nightwatch-html-reporter');
  var reporter = new htmlReporter({
    openBrowser: false,
    reportFilename: 'report_' + getDateCustomFormat() + '.html',
    uniqueFilename: false,
    separateReportPerSuite: false,
    hideSuccess: true,
    themeName: 'default',
    relativeScreenshots: true,
    reportsDirectory: __dirname.replace('conf','') + 'reports/html'
  });

  return reporter.fn;
}

var self = module.exports = {

  abortOnAssertionFailure: false,
  waitForConditionTimeout : 4000,
  reporter: initHtmlReport(),
  assert: require('assert'),
  nodemailer: require('nodemailer'),
  imaps: require('imap-simple'),
  config: config,
  testData: {},

  before: function(done) {
    done();
  },

  beforeEach: function(done) {
    done();
  },

  afterEach: function(done) {
    done();
  },

  after: function(done) {
    done();
  }
}

