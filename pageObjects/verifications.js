var verificationsCommands = {
  receive: function(browser) {
    var users = browser.page.data.users();
    var mails = browser.page.data.mails();

    browser.page.mail()
      .waitForElementVisible('@email')
    browser.page.mail()
      .expect.element('@email').text.to.contains( users.first.email ).before();
    browser.page.mail()
      .expect.element('@email').text.to.contains( mails.message.subject ).before();
    browser.page.mail()
      .expect.element('@email').text.to.contains( mails.message.text ).before();
  },

  receiveResponse: function(browser) {
    var users = browser.page.data.users();
    var mails = browser.page.data.mails();

    browser.page.mail()
      .waitForElementVisible('@email')
    browser.page.mail()
      .expect.element('@email').text.to.contains( 'я, ' + users.second.name).before();
    browser.page.mail()
      .expect.element('@email').text.to.contains( mails.message.subject ).before();
    browser.page.mail()
      .expect.element('@email').text.to.contains( mails.response.text ).before();
  }
}

module.exports = {
  commands: [verificationsCommands],

  url: '',

  elements: { }
}