var commonCommands = {
  cleanUpAll: function(browser) {
    var users = browser.page.data.users();

    browser.log('-x-x-x-x-x-x-x-x-x-')
    browser.log('Run clean up')
    browser.log('-x-x-x-x-x-x-x-x-x-')

    Object.keys(users).forEach(function(name, key) {
      browser.page.mail().isEmailsExist(browser, users[name], function(result) {
        users[name].emailsExist = result;
      })
    })

    browser.log('Pause for check exist emails')
    browser.pause(browser.globals.waitForConditionTimeout, function() {
      Object.keys(users).forEach(function(name, key) {
        if (users[name].emailsExist) {
          browser.log('Delete all emails for user - ' + users[name].email)
          browser.page.common().navigateUrl()
          browser.page.autorization().login(browser, users[name])
          browser.page.mail().deleteAllEmails(browser)
          browser.page.autorization().logout(browser)
        } else {
          browser.log('No emails exist for email ' + users[name].email)
        }
      })
    })
  },

  isExistElement: function(element, callback) {
    this.api.element( element.locateStrategy, element.selector, function(result) {
      var message = 'Element ' + element.selector
      message = result.status !== -1 ?  message + ' is exist' : message + ' is not exist'
      this.log(message)
      callback( (result.status !== -1) );
    })
  },

  navigateUrl: function(url = this.api.launchUrl) {
    var self = this;
    self.log('Navigate to url - ' + url)
    self.navigate(url)
    self.waitForElementVisible('body')
  },
}

module.exports = {
  commands: [commonCommands],

  url: function () {
    return this.api.launchUrl;
  },

  elements: { }
}