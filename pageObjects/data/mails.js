module.exports = {
  message: {
    subject: 'First subject message',
    text: 'First text message'
  },
  response: {
    subject: 'Response test subject message',
    text: 'Response test text message'
  }
}