var util = require('util');
var events = require('events');

function Log () {
  events.EventEmitter.call(this);
};

util.inherits(Log, events.EventEmitter)

Log.prototype.command = function(message) {
  var self = this;
  self.client.api.pause(1, function() {
    self.client.api.verify.ok('Info', message);
    self.emit('complete');
    return this;
  })
};

module.exports = Log;