module.exports = {
  protocolConfig: {
    imaphost: 'imap.gmail.com',
    imapport: 993,
    smtphost: 'smtp.gmail.com'
  },
  urls: {
    "gmail": "https://mail.google.com/mail/u/0/",
    "logout": "https://accounts.google.com/Logout"
  }
}