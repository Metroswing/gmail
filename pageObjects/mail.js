var mailCommands = {
  send: function(browser) {
    var protocolConfig = browser.globals.config.protocolConfig;
    var users = browser.page.data.users();
    var mails = browser.page.data.mails();
    var smtpConnectionString = 'smtps://' + users.first.email + ':' + users.first.pass + '@' + protocolConfig.smtphost;
    var transporter = browser.globals.nodemailer.createTransport(smtpConnectionString);
    var options = {
      from: users.first.email,
      to: users.second.email,
      subject: mails.message.subject,
      text: mails.message.text,
      html: mails.message.html
    };

    // Send message
    transporter.sendMail(options, function(error, info){
      if(error){ 
        browser.log( 'Error during send email - ' + error);
      }
      browser.log('Message sent: ' + info);
    });
    browser.log('Pause for send emails')
    browser.pause(browser.globals.waitForConditionTimeout * 1.5)
  },

  sendResponse: function(browser) {
    var mails = browser.page.data.mails();
    
    browser.page.mail()
      .waitForElementVisible('@email')
        .clickWithLog('@email')
      .waitForElementVisible('@replyButton')
        .clickWithLog('@replyButton')
      .waitForElementVisible('@responseTextInput')
        .setValueWithLog('@responseTextInput', mails.response.text)
      .waitForElementVisible('@sendReplyButton')
        .clickWithLog('@sendReplyButton')
      .waitForElementVisible('@messageSentNotification', browser.globals.waitForConditionTimeout * 2)
  },

  deleteAllEmails: function(browser) {
    browser.page.mail()
      .waitForElementVisible('@checkBoxAll')
        .clickWithLog('@checkBoxAll')
      .waitForElementVisible('@deleteAllButton')
        .clickWithLog('@deleteAllButton')
    browser.page.mail()
      .waitForElementVisible('@noNewEmails')
    browser.log('Pause for delete emails')
    browser.pause(browser.globals.waitForConditionTimeout * 1.5)
  },

  isEmailsExist: function(browser, user, callback) {
    var protocolConfig = browser.globals.config.protocolConfig;

    var config = {
      imap: {
        user: user.email,
        password: user.pass,
        host: protocolConfig.imaphost,
        port: protocolConfig.imapport,
        tls: true,
        authTimeout: 3000
      }
    };

    browser.log('Check exist new emails for user - ' + user.email)
    browser.globals.imaps.connect(config).then(function (connection) {
      return connection.openBox('INBOX').then(function () {
        var searchCriteria = ['ALL'];
        var fetchOptions = {
            bodies: ['HEADER', 'TEXT'],
            markSeen: false
        };

        return connection.search(searchCriteria, fetchOptions).then(function (results) {
          connection.end()
          callback( (results.length > 0) );
        });
      });
    });
  }

}

module.exports = {
  commands: [mailCommands],

  url: '',

  elements: {
    inbox:  {
      selector: 'div[id=":2"] table[cellpadding="0"]'
    },
    email: {
      selector: 'div[id=":2"] table tr'
    },
    replyButton: {
      locateStrategy: 'xpath',
      selector: '//table[@role="presentation"]//span[text()[contains(., "Ответить")] and @role="link"]'
    },
    sendReplyButton: {
      locateStrategy: 'xpath',
      selector: '//table[@role="presentation"]//div[text()[contains(., "Отправить")]]'
    },
    responseTextInput: {
      selector: 'div.editable'
    },
    checkBoxAll: {
      selector: 'div[id=":5"] span[aria-checked="false"]'
    },
    deleteAllButton: {
      selector: 'div[id=":5"] div[aria-label="Удалить"]'
    },
    noNewEmails: {
      locateStrategy: 'xpath',
      selector: '//div[@id=":2"]//table//td[text()="Новых писем нет"]'
    },
    messageSentNotification: {
      locateStrategy: 'xpath',
      selector: '//div[text()="Письмо отправлено."]'
    }
  }
}