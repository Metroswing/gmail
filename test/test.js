var self = module.exports = {
  beforeEach: function(browser) {
    browser.page.common().cleanUpAll(browser)
  },

  afterEach: function(browser, done) {
    browser.end(function() { done() });
  },

  'Send, receive, response email': function(browser) {
    browser.log('-x-x-x-x-x-x-x-x-x-')
    browser.log('Run test')
    browser.log('-x-x-x-x-x-x-x-x-x-')

    var users = browser.page.data.users();
    browser.pause(1, function() {
      browser.page.mail().send(browser)
    })

    browser.page.common().navigateUrl()
    browser.page.autorization().login(browser, users.second)
    browser.page.verifications().receive(browser)
    browser.page.mail().sendResponse(browser)
    browser.page.autorization().logout(browser)

    browser.page.common().navigateUrl()
    browser.page.autorization().login(browser, users.first)
    browser.page.verifications().receiveResponse(browser)
    browser.page.mail().deleteAllEmails(browser)
  }
}