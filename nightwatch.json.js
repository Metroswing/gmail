var serverPath = require('selenium-server-standalone'),
    chromedriver = require('chromedriver'),
    config = require('./conf/config');

module.exports = {
  "src_folders" : "tests",
  "output_folder" : "reports",
  "custom_assertions_path" : [],
  "custom_commands_path": ["./lib/customCommands/"],
  "page_objects_path" : "./pageObjects/",
  "globals_path" : "./conf/global.js",
  "test_workers": false,

  "selenium" : {
    "start_process" : true,
    "server_path" : serverPath,
    "log_path" : "reports",
    "host" : "127.0.0.1",
    "port" : 4444,
    "cli_args" : {
      "webdriver.chrome.driver" : chromedriver.path
    }
  },

  "test_settings" : {
    "default" : {
      "launch_url" : config.urls.gmail,
      "selenium_port"  : 4444,
      "selenium_host"  : "localhost",
      "silent": true,
      "persist_globals": true,
      "screenshots" : {
        "enabled": true,
        "on_failure": true,
        "path": "./reports/screen-shots"
      },
      "desiredCapabilities": {
        "browserName": "chrome",
        "javascriptEnabled": true,
        "acceptSslCerts": true,
        "chromeOptions" : {
          "args" : ["--window-size=1600,1000"],
          "prefs": {
            "safebrowsing": {
              "disable": "true",
            }
         },
        },
      },
      "skip_testcases_on_fail": false
    }
  }
}