var autorizationCommands = {
  login: function(browser, user) {
    browser.page.autorization()
      .waitForElementVisible('@emailInput')
        .setValueWithLog('@emailInput', user.email)
          .clickWithLog('@nextButton')
      .waitForElementVisible('@passInput')
        .setValueWithLog('@passInput', user.pass)
      .waitForElementVisible('@passwordNextButton')
        .clickWithLog('@passwordNextButton')
    browser.page.mail()
      .waitForElementVisible('@inbox', 10000)
  },

  logout: function(browser) {
    browser.page.common()
      .navigateUrl( browser.globals.config.urls.logout )
    browser.deleteCookie()
  }
}

module.exports = {
  commands: [autorizationCommands],

  url: '',

  elements: {
    emailInput:  {
      selector: 'input[id="identifierId"]'
    },
    passInput:  {
      selector: 'input[name="password"]'
    },
    nextButton: {
      selector: 'div[id="identifierNext"]'
    },
    passwordNextButton:  {
      selector: 'div[id="passwordNext"]'
    },
  }
}